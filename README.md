# Pre-Requisites for Ansible setup
- Jenkins started up
- Kubectl and Helm installed
- ECR setup
### Run these pipelines in order

- After deploying the ECR, run these pipelines
```
http://<jenkins-public-ip>:7777/job/ansible-docker/
```
- Deploy EKS and then run this pipeline
```
http://<jenkins-public-ip>:7777/job/ansible-monitoring/
```
### Setup Dashboards after installation

- To Setup dashboards, login to Grafana and select the import tab on the left. The add the following codes to load in:

```
3119
6417
Select ‘Prometheus’ as the endpoint under prometheus data sources drop downs
Click ‘Import’

```